# Copyright 2024, Thomas Juerges
# SPDX-License-Identifier: BSD-3-Clause
"""
Use this device proxy with zeroconf support support like this:
from tango_zeroconf import ZeroconfDeviceProxy
zp = ZeroconfDeviceProxy()
device = "foo/bar/1"
device_info = zp.get_device_info(device)
print(f"{device_info}")
dp = zp.connect(device)
print(f"{dp.get_attribute_list()}")
print(f"{dp.read_attribute("state")}")
"""

from tango import DevFailed, DeviceProxy  # type: ignore[import-untyped]
from .zc_config import TangoZeroconfServiceInfoKeys
from .zc_listener import ZeroconfListener


def p(what):
    print(f"Exception:\n{what}")


class ZeroconfDeviceProxy:
    def __init__(self, *args, **kwargs):
        self.__args = args
        self.__kwargs = kwargs
        self.__zeroconf_listener: ZeroconfListener = ZeroconfListener()  # type: ignore[annotation-unchecked]
        self.__device_proxy: DeviceProxy | None = None  # type: ignore[annotation-unchecked]

    def get_device_info(self, device: str, timeout: float = 3.0) -> dict:
        return self.__zeroconf_listener.get_device_info(device, timeout)

    def connect(self, name: str, timeout: float = 3.0) -> DeviceProxy | None:
        """Return the available devices matching the name parameter.

        Args:
            name (str): Name of the devices to be serached for.
            timeout (float): Timeout in s for a network query if then device
            cannot be found in the cache.
        Returns:
            dict: Device info as published on the network.
        """
        device_info: dict = self.get_device_info(name, timeout)
        if not isinstance(device_info, dict):
            return None
        self.__device_proxy = None
        for trl in device_info[TangoZeroconfServiceInfoKeys.FULL_TRL.value]:
            try:
                print(f"Connecting to {name} at {trl}...")
                self.__device_proxy = DeviceProxy(f"{trl}")
                self.__device_proxy.read_attribute("state")
                print(f"Successfully connected to {name} at {trl}.")
                break
            except DevFailed:
                print(f"Failed connecting to {name} at {trl}.")
                pass
        return self.__device_proxy
