# Copyright 2024, Thomas Juerges
# SPDX-License-Identifier: BSD-3-Clause
from socket import inet_ntoa
from ast import literal_eval
from zeroconf import (
    DNSQuestionType,
    ServiceBrowser,
    ServiceInfo,
    ServiceListener,
    Zeroconf,
)

from tango_zeroconf.zc_config import (
    TangoZeroconfIPVersion,
    TangoZeroconfServiceInfoKeys,
    TANGO_ZEROCONF_DEFAULT_SERVICE,
    create_tango_service_name,
)


class ZeroconfListener(ServiceListener):
    """_summary_

    Args:
        ServiceListener (_type_): _description_
    """

    def __init__(self, zc: Zeroconf | None = None):
        if zc:
            self.__zeroconf = zc
        else:
            self.__zeroconf = Zeroconf(ip_version=TangoZeroconfIPVersion)
        self.available_services: dict = {}
        ServiceBrowser(self.__zeroconf, TANGO_ZEROCONF_DEFAULT_SERVICE, listener=self)

    def convert_service_info_to_dict(self, service_info: ServiceInfo) -> dict:
        device_info: dict = {}
        for i in TangoZeroconfServiceInfoKeys:
            try:
                # Convert everything back to Python types. If the coversion
                # fails, just store what we received.
                value = service_info.decoded_properties.get(i.value, None)
                device_info[i.value] = literal_eval(value)
            except Exception:
                device_info[i.value] = service_info.decoded_properties.get(
                    i.value, None
                )

        addresses = list()
        for i in service_info.addresses:
            addresses.append(inet_ntoa(i))
        device_info[TangoZeroconfServiceInfoKeys.ZC_ADDRESSES.value] = addresses
        device_info[TangoZeroconfServiceInfoKeys.ZC_INTERFACE_INDEX.value] = (
            service_info.interface_index
        )
        device_info[TangoZeroconfServiceInfoKeys.ZC_NAME.value] = service_info.name
        device_info[TangoZeroconfServiceInfoKeys.ZC_PORT.value] = service_info.port
        device_info[TangoZeroconfServiceInfoKeys.ZC_PRIORITY.value] = (
            service_info.priority
        )
        device_info[TangoZeroconfServiceInfoKeys.ZC_TYPE.value] = service_info.type
        device_info[TangoZeroconfServiceInfoKeys.ZC_WEIGHT.value] = service_info.weight
        if len(device_info) != len(TangoZeroconfServiceInfoKeys):
            print(
                f"There appears to be a discrepancy between the expected number of Zeroconf items for device {device_info[TangoZeroconfServiceInfoKeys.DEV_NAME.value]}: Expected = {len(TangoZeroconfServiceInfoKeys)}, received = {len(device_info)}"
            )
        return device_info

    def get_device_info(self, device_name: str, timeout: float = 3.0) -> dict:
        device: dict = self.available_services.get(device_name, {})
        if len(device) > 0:
            return device
        name = create_tango_service_name(device_name)
        type_ = TANGO_ZEROCONF_DEFAULT_SERVICE
        print(
            f"Will query for device {device_name} (zc_name = {name}, zc_type = {type_}) with timeout = {timeout}s."
        )
        reply = self.__zeroconf.get_service_info(
            type_=type_,
            name=name,
            timeout=int(timeout * 1000),
            question_type=DNSQuestionType.QU,
        )
        if isinstance(reply, ServiceInfo):
            print(
                f"Received a reply from the device {device_name} (zc_name = {name}, zc_type = {type_}). Will add it to the local cache."
            )
            self.available_services[name] = self.convert_service_info_to_dict(reply)
        else:
            print(
                f"The device {name} (zc_name = {name}, zc_type = {type_}) is unreachable or its Zeroconf service does not reply. This means that I cannot fetch its Zeroconf service information."
            )
            return {}
        device = self.available_services.get(name)
        return device

    def add_service(self, zc: Zeroconf, type_: str, name: str) -> None:
        print(f"Service {name} has been started, service type: {type_}.")
        device_name = name.replace(f".{type_}", "")
        info = zc.get_service_info(type_, name)
        if info is not None:
            self.available_services[device_name] = self.convert_service_info_to_dict(
                info
            )
            print(
                f"Info for device {device_name} (zc_name = {name}, zc_type = {type_}) has been added to the local cache."
            )
        else:
            print(
                f"Could not get the Zeroconf service info for the device {device_name} (zc_name = {name}, zc_type = {type_}). This is unusual and should not happen. I cannot add the device information to the cache. Please check that the device is running and that it can be reached over the network and try again."
            )

    def remove_service(self, zc: Zeroconf, type_: str, name: str) -> None:
        device_name = name.replace(f".{type_}", "")
        if device_name in self.available_services:
            print(
                f"Removing device {device_name} (zc_name = {name}, zc_type = {type_})"
            )
            self.available_services.pop(device_name)
            print(
                f"Service {name}, type {type_}, has been stopped, device {device_name} was removed from cache."
            )
        else:
            print(
                f"The device {device_name} (zc_name = {name}, zc_type = {type_}) is not in the local cache and hence cannot be removed. This is unexpected, but since the device is not in the cache anyway, I will continue with the normal execution."
            )

    def update_service(self, zc: Zeroconf, type_: str, name: str) -> None:
        print(f"Service {name} of type {type_} state changed")
        device_name = name.replace(f".{type_}", "")
        info = zc.get_service_info(type_, name)
        if info is not None:
            self.available_services[device_name] = self.convert_service_info_to_dict(
                info
            )
            print(
                f"THe info in the local cache for the device {device_name} (zc_name = {name}, zc_type = {type_}) has been updated."
            )
        else:
            print(
                f"Did not receive the Zeroconf service info for the device {device_name} (zc_name = {name}, zc_type = {type_}). This is unusual and should not happen. Will not update any existing info that is already in the cache."
            )
