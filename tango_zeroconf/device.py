# Copyright 2024, Thomas Juerges
# SPDX-License-Identifier: BSD-3-Clause
"""Implementation of a Tango Device with DNS-SD support via zeroconf.
"""

from socket import gethostbyname, inet_aton
from os import getenv
from tango import DeviceClass, Util, test_context  # type: ignore[import-untyped]
import tango.server  # type: ignore[import-untyped]
from zeroconf import (
    ServiceInfo,
    Zeroconf,
)
from .zc_config import (
    TangoZeroconfIPVersion,
    TANGO_ZEROCONF_DEFAULT_SERVICE,
    TangoZeroconfServiceInfoKeys,
    create_tango_service_name,
)
from .zc_listener import ZeroconfListener


attribute = tango.server.attribute
command = tango.server.command
device_property = tango.server.device_property
class_property = tango.server.class_property
run = tango.server.run
server_run = tango.server.server_run


def p(what):
    print(f"Exception:\n{what}")


class ZeroconfDevice(tango.server.Device):
    """A Tango Device with DNS-SD support.
    The device announces its own capabilities via a single DNS-SD broadcast.
    When it receives a broadcast that queries it specifically, it will reply
    with a broadcast again.

    Args:
        Device (_type_): Tango Device class.
    """

    def __init__(self, cl, name):  # type: ignore[annotation-unchecked]
        super().__init__(cl, name)
        self.__zeroconf: Zeroconf | None = None
        self.__zeroconf_service_info: ServiceInfo | None = (
            None  # ignore: type[annotation-unchecked]
        )
        self.__zeroconf_listener: ZeroconfListener | None = (
            None  # ignore: type[annotation-unchecked]
        )

    def __create_zeroconf_service_properties(self) -> dict:
        """_summary_
        Create a device description that contains everything
        that is relevant for clients to know.

        Returns:
            dict: The property dict for a Zeroconf ServiceInfo.
        """
        zeroconf_service_properties: dict = {}

        # Get TANGO_HOST from the environment.
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.TANGO_HOST.value] = (
            getenv("TANGO_HOST", None)
        )

        # Should use the doc string.
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.DESCRIPTION.value] = (
            "This is the first DNS-SD PyTango device"
        )

        # I need the hostname.
        util: Util = Util.instance()
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.HOST.value] = (
            util.get_host_name()
        )

        # To get the CORBA port that the device server is using. I simply
        # re-use what PyTango does in tango.test_context.
        # I'll fetch some info from the device class and Util.
        ds = util.get_dserver_device()
        encoded_ior = util.get_dserver_ior(ds)
        ior = test_context.parse_ior(encoded_ior)
        # This almost always fails because the returned dict has randomly
        # entries filled with bytes that cannot be decoded to str. :-(
        # And the weirdest thing is that one cannot catch the exception!
        #
        # zeroconf_service_properties[TangoZeroconfServiceInfoKeys.IOR.value] = str(
        #     ior.body, errors="replace"
        # )
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.IOR.value] = None

        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.PORT.value] = ior.port

        # Add the device's TRL.
        device_name = self.get_name()
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.DEV_NAME.value] = (
            device_name
        )

        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.TRL.value] = (
            device_name
        )

        # And with the host and port I can also provide a the two TRLs:
        # One that allows direct access and the other one that goes through
        # the TangoDB.
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.FULL_TRL.value] = (
            f"tango://{zeroconf_service_properties[TangoZeroconfServiceInfoKeys.HOST.value]}:{zeroconf_service_properties[TangoZeroconfServiceInfoKeys.PORT.value]}/{zeroconf_service_properties[TangoZeroconfServiceInfoKeys.TRL.value]}#dbase=no",
            f"tango://{zeroconf_service_properties[TangoZeroconfServiceInfoKeys.TANGO_HOST.value]}/{zeroconf_service_properties[TangoZeroconfServiceInfoKeys.TRL.value]}",
        )

        # Add the attribute list
        device_class: DeviceClass = self.get_device_class()
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ATTRIBUTES.value] = (
            device_class.attr_list.keys()
        )

        # Add the command list.
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.COMMANDS.value] = (
            device_class.cmd_list.keys()
        )

        # Add the class property list.
        zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.CLASS_PROPERTIES.value
        ] = device_class.class_property_list.keys()

        # Add the device property list.
        zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.DEVICE_PROPERTIES.value
        ] = device_class.device_property_list.keys()

        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.DS_NAME.value] = (
            util.get_ds_name()
        )

        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.EXEC_NAME.value] = (
            util.get_ds_exec_name()
        )

        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.INST_NAME.value] = (
            util.get_ds_inst_name()
        )

        zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.IS_AUTO_ALARM_ON_CHANGE_EVENT.value
        ] = util.is_auto_alarm_on_change_event()

        zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.IS_KERNEL_TRACING_ENABLED.value
        ] = self.is_kernel_tracing_enabled()

        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.IS_POLLED.value] = (
            self.is_polled()
        )

        zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.IS_TELEMETRY_ENABLED.value
        ] = self.is_telemetry_enabled()

        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.PID.value] = (
            util.get_pid_str()
        )

        zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.POLL_RING_DEPTH.value
        ] = self.get_poll_ring_depth()

        zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.SERVER_VERSION.value
        ] = util.get_server_version()

        # This almost always fails because the returned dict has randomly
        # entries filled with bytes that cannot be decoded to str. :-(
        # And the weirdest thing is that one cannot catch the exception!
        # zeroconf_service_properties[TangoZeroconfServiceInfoKeys.VERSION_INFO.value] = (
        #     self.get_version_info()
        # )
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.VERSION_INFO.value] = (
            None
        )

        zeroconf_service_properties = dict(sorted(zeroconf_service_properties.items()))
        return zeroconf_service_properties

    def __create_zeroconf_service_info(
        self, zeroconf_service_properties: dict
    ) -> ServiceInfo:
        # Create the zeroconf service info structure.
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_TYPE.value] = (
            TANGO_ZEROCONF_DEFAULT_SERVICE
        )
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_NAME.value] = (
            create_tango_service_name(
                zeroconf_service_properties[TangoZeroconfServiceInfoKeys.DEV_NAME.value]
            )
        )
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_ADDRESSES.value] = (
            gethostbyname(
                zeroconf_service_properties[TangoZeroconfServiceInfoKeys.HOST.value]
            )
        )
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_PORT] = (
            zeroconf_service_properties[TangoZeroconfServiceInfoKeys.PORT.value]
        )
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_SERVER.value] = (
            f"{zeroconf_service_properties[TangoZeroconfServiceInfoKeys.HOST.value]}.local."
        )
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_WEIGHT.value] = 0
        zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_PRIORITY.value] = 0
        type_ = zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_TYPE.value]
        name = zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_NAME.value]
        addresses = [
            inet_aton(
                zeroconf_service_properties[
                    TangoZeroconfServiceInfoKeys.ZC_ADDRESSES.value
                ]
            )
        ]
        port = zeroconf_service_properties[TangoZeroconfServiceInfoKeys.ZC_PORT.value]
        properties = zeroconf_service_properties
        server = zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.ZC_SERVER.value
        ]
        weight = zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.ZC_WEIGHT.value
        ]
        priority = zeroconf_service_properties[
            TangoZeroconfServiceInfoKeys.ZC_PRIORITY.value
        ]
        service_info = ServiceInfo(
            type_=type_,
            name=name,
            addresses=addresses,
            port=port,
            properties=properties,
            server=server,
            weight=weight,
            priority=priority,
        )
        return service_info

    def __create_zeroconf_service(self) -> ServiceInfo:
        # Create the zeroconf service and register it.
        zeroconf_service_properties = self.__create_zeroconf_service_properties()
        zeroconf_service_info = self.__create_zeroconf_service_info(
            zeroconf_service_properties
        )
        self.__zeroconf.register_service(zeroconf_service_info)
        return zeroconf_service_info

    def __destroy_zeroconf_service(self, service_info: ServiceInfo) -> None:
        # Unregister the zeroconf service.
        if self.__zeroconf and service_info:
            self.__zeroconf.unregister_service(service_info)

    def __create_zeroconf_listener(self) -> ZeroconfListener:
        return ZeroconfListener(self.__zeroconf)

    def __destroy_zeroconf_listener(self) -> None:
        """Nothing needs to be done. The listener is destroyed when the zeroconf object is destroyed."""
        self.__zeroconf_listener = None

    def __start_zeroconf(self) -> None:
        self.__zeroconf = Zeroconf(ip_version=TangoZeroconfIPVersion)
        self.__zeroconf_listener = self.__create_zeroconf_listener()
        self.__zeroconf_service_info = self.__create_zeroconf_service()

    def __stop_zeroconf(self) -> None:
        self.__destroy_zeroconf_service(self.__zeroconf_service_info)
        self.__destroy_zeroconf_listener()
        if self.__zeroconf:
            self.__zeroconf.close()
            self.__zeroconf = None

    def server_init_hook(self) -> None:
        super().server_init_hook()
        try:
            self.__start_zeroconf()
        except Exception as ex:
            p(ex)

    def delete_device(self) -> None:
        try:
            self.__stop_zeroconf()
        except Exception as ex:
            p(ex)
        super().delete_device()
