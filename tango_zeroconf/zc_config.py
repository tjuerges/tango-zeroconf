# Copyright 2024, Thomas Juerges
# SPDX-License-Identifier: BSD-3-Clause
from enum import StrEnum, unique
from zeroconf import IPVersion


TangoZeroconfIPVersion = IPVersion.All


@unique
class TangoZeroconfServices(StrEnum):
    """_summary_

    Args:
        Enum (_type_): Protocols that Tango devices can announce.
    """

    TLS = "_tango._tls.local."
    TCP = "_tango._tcp.local."
    UDP = "_tango._udp.local."
    HTTP = "_tango._http.local."
    HTTPS = "_tango._https.local."


TANGO_ZEROCONF_DEFAULT_SERVICE = TangoZeroconfServices.TCP.value


def create_tango_service_name(device_name: str) -> str:
    return f"{device_name}.{TANGO_ZEROCONF_DEFAULT_SERVICE}"


# Would be nice if one could have a dict with immutable keys.
# For the time being this has to do.
class TangoZeroconfServiceInfoKeys(StrEnum):
    ATTRIBUTES = "attributes"
    CLASS_PROPERTIES = "class_properties"
    COMMANDS = "commands"
    DESCRIPTION = "description"
    DEV_NAME = "dev_name"
    DEVICE_PROPERTIES = "device_properties"
    DS_NAME = "ds_name"
    EXEC_NAME = "exec_name"
    FULL_TRL = "full_trl"
    HOST = "host"
    INST_NAME = "inst_name"
    IOR = "ior"
    IS_AUTO_ALARM_ON_CHANGE_EVENT = "is_auto_alarm_on_change_event"
    IS_KERNEL_TRACING_ENABLED = "is_kernel_tracing_enabled"
    IS_POLLED = "is_polled"
    IS_TELEMETRY_ENABLED = "is_telemetry_enabled"
    PID = "pid"
    POLL_RING_DEPTH = "poll_ring_depth"
    PORT = "port"
    SERVER_VERSION = "server_version"
    TANGO_HOST = "tango_host"
    TRL = "trl"
    VERSION_INFO = "version_info"
    ZC_ADDRESSES = "zc_addresses"
    ZC_INTERFACE_INDEX = "zc_interface_index"
    ZC_NAME = "zc_name"
    ZC_PORT = "zc_port"
    ZC_PRIORITY = "zc_priority"
    ZC_SERVER = "zc_server"
    ZC_TYPE = "zc_type"
    ZC_WEIGHT = "zc_weight"
