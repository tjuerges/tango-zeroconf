#! /usr/bin/env bash
# Copyright 2024, Thomas Juerges
# SPDX-License-Identifier: BSD-3-Clause

# Check if the TangoDB environment variable TANGO_HOST is set.
# If it is not set, run the DeviceServer without TangoDB.
if [ -z ${TANGO_HOST} ]; then
    # Run the ZeroConf Tango DeviceServer without TangoDB
    echo "Will start the DeviceServer in nodb mode."
    python3 tests/ZeroconfExampleDevice.py test -v4 -nodb -port 45678 -dlist test/zeroconf/1,test/zeroconf/2;
else
    # Check if the devices are already defined in the TangoDB. If not define them.
    tango_admin --check-device test/zeroconf/1
    [[ ${?} -ne 0 ]] && { tango_admin --add-server ZeroconfExampleDevice/test ZeroconfExampleDevice test/zeroconf/1,test/zeroconf/2; }

    # Run the ZeroConf Tango DeviceServer.
    echo "Will start the DeviceServer in full TangoDB mode."
    python3 tests/ZeroconfExampleDevice.py test -v4
fi
