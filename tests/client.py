#! /usr/bin/env python3
# Copyright 2024, Thomas Juerges
# SPDX-License-Identifier: BSD-3-Clause
from tango_zeroconf.proxy import ZeroconfDeviceProxy
from tango_zeroconf.zc_config import TangoZeroconfServiceInfoKeys

zp = ZeroconfDeviceProxy()
device = "test/zeroconf/1"
info = zp.get_device_info(device, 1.0)
if len(info) > 0:
    print(
        f"***\nDevice info for device {device}:\n{info[TangoZeroconfServiceInfoKeys.FULL_TRL.value]}\n***"
    )
    dp = zp.connect(device)
    if dp is not None:
        print(f"{dp.get_attribute_list()}")
        print(f"{dp.get_command_list()}")
        print(f"{dp.command_inout("state")}")
else:
    print(f"The device {device} could not be found. Please verify that it is running.")
