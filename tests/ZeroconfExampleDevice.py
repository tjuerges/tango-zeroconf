#! /usr/bin/env python3
# Copyright 2024, Thomas Juerges
# SPDX-License-Identifier: BSD-3-Clause
"""
Run this Python3 script on your local machine like this:
python3 ZeroconfDevice.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
"""

from sys import argv
from tango import DevState
from tango_zeroconf.device import (
    ZeroconfDevice,
    attribute,
    command,
    run,
)


def p(what):
    print(f"Exception:\n{what}")


class ZeroconfExampleDevice(ZeroconfDevice):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321
        self.set_state(DevState.ON)

    def delete_device(self):
        self.set_state(DevState.OFF)
        super().delete_device()

    @attribute()
    def my_ro_attribute(self) -> float:
        return self.__my_ro_attribute_value

    @attribute()
    def my_rw_attribute(self) -> float:
        return self.__my_rw_attribute_value

    @my_rw_attribute.write
    def my_rw_attribute(self, value: float = None) -> None:
        self.__my_rw_attribute_value = value

    @command()
    def set_my_ro_attribute(self, value: float) -> None:
        self.__my_ro_attribute_value = value

    @command()
    def read_my_rw_attribute(self) -> float:
        return self.__my_rw_attribute_value

    @command()
    def read_my_type_hint_rw_attribute(self) -> float:
        return self.__my_rw_attribute_value


def main():
    run((ZeroconfExampleDevice,), argv)


if __name__ == "__main__":
    main()
